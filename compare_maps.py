#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os
import math
# import cv2
from skimage.io import imread
from skimage import data, img_as_float
from skimage.metrics import structural_similarity as ssim
from skimage.metrics import mean_squared_error as mse
import pathlib
from skimage.color import rgb2gray, rgba2rgb
from scipy import stats
import cv2


from sklearn.cluster import KMeans



def compare_images(imageA, imageB, imageC, title):
    # compute the mean squared error and structural similarity
    # index for the images
    m = mse(imageA, imageB)
    s = ssim(imageA, imageB, multichannel=True)

    m2 = mse(imageA, imageC)
    s2 = ssim(imageA, imageC, multichannel=True)

    m3 = mse(imageB, imageC)
    s3 = ssim(imageB, imageC, multichannel=True)
    # setup the figure
    fig = plt.figure(title)
    plt.suptitle("With AE - MSE: %.2f, SSIM: %.2f \n Without AE - MSE: %.2f, SSIM: %.2f \n Between generated - MSE: %.2f, SSIM: %.2f " % (m, s, m2, s2, m3, s3))
    # show first image
    ax = fig.add_subplot(1, 3, 1)
    plt.imshow(imageA, cmap=plt.cm.gray)
    plt.title('Ground truth')
    plt.axis("off")
    # show the second image
    ax = fig.add_subplot(1, 3, 2)
    plt.imshow(imageB, cmap=plt.cm.gray)
    plt.title('With AE')
    plt.axis("off")

    ax = fig.add_subplot(1, 3, 3)
    plt.imshow(imageC, cmap=plt.cm.gray)
    plt.title('Without AE')
    plt.axis("off")
    # show the images
    # plt.show()
    if save:
        fig.savefig(save_dir+title+'.png')
        plt.close(fig)
    else:
        plt.show()

    return [[m, s], [m2, s2], [m3, s3]]

def compare_maps(testcase):
    scene = testcase[0]
    tc = testcase[1]

    if tc[0] == 'n':
        truth = map_dir + '/' + map_batch + '_maps/ground_truths/Scene' + scene[0] + '_' + tc[0:2] + '.png'
    else: 
        truth = map_dir + '/' + map_batch + '_maps/ground_truths/Scene' + scene[0] + '_' + tc + '.png'

    with_ae = map_dir + '/' + map_batch + '_maps/' + thresh + '/limB_AE_' + scene + '_' + tc + '.png'
    without_ae = map_dir + '/' + map_batch + '_maps/' + thresh + '/no_AE_' + scene + '_' + tc + '.png'

    ground_truth = imread(truth)
    map_with_ae = imread(with_ae)
    map_without_ae = imread(without_ae)

    print(scene, tc, ground_truth.shape, map_with_ae.shape, map_without_ae.shape)

    try: 
        results = compare_images(ground_truth, map_with_ae, map_without_ae, scene + '_' + tc)
    except:
        print('Special case')

        if tc[0] == 'n':
            truth = map_dir + '/' + map_batch + '_maps/ground_truths/Scene' + scene[0] + '_' + tc[0:2] + '_special.png'
        else: 
            truth = map_dir + '/' + map_batch + '_maps/ground_truths/Scene' + scene[0] + '_' + tc + '_special.png'


        
        ground_truth = imread(truth)
        results = compare_images(ground_truth, map_with_ae, map_without_ae, scene + '_' + tc)



    f= open(map_dir + "/results/" + results_file + ".csv","a+")
    #        ('case \t          mse_ae \t                   mse_none \t                 mse_gen \t                   ssim_ae \t                   ssim_none \t               ssim_gen \n')
    f.write(scene + tc + '\t' + str(results[0][0]) + '\t' + str(results[1][0]) + '\t' + str(results[2][0]) + '\t'  + str(results[0][1]) + '\t' + str(results[1][1]) + '\t' + str(results[2][1]) + '\n')
    f.close()

#########################

map_dir = os.path.expanduser('~/eval/maps')


results_file = "mse_ssim_ABC"
map_batch = "ABC"
thresh = "default_thresh"

save_dir = os.path.expanduser('~/eval/maps/compared_maps/abc/')
save = True
pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True)


f= open(map_dir + "/results/" + results_file + ".csv","w+")
f.write('case \t mse_ae \t mse_none \t mse_gen \t ssim_ae \t ssim_none \t ssim_gen \n')
f.close()

for scene in ['Aa', 'Ab', 'Ac', 'Ba', 'Bb', 'Bc', 'Ca']:
    for tc in ['nc1', 'nc2', 'nc3', 'tc1', 'tc2', 'tc3']:

        compare_maps([scene, tc])

for scene in ['Cb', 'Cc']:
    for tc in ['nc1', 'nc2', 'nc3']:
        compare_maps([scene, tc])

for scene in ['Cb']:
    for tc in ['tc1']:
        compare_maps([scene, tc])
        

results_file = "mse_ssim_DEF"
map_batch = "DEF"
thresh = "default_thresh"

save_dir = os.path.expanduser('~/eval/maps/compared_maps/def/')
save = True
pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True)


f= open(map_dir + "/results/" + results_file + ".csv","w+")
f.write('case \t mse_ae \t mse_none \t mse_gen \t ssim_ae \t ssim_none \t ssim_gen \n')
f.close()

for scene in ['D', 'E', 'F']:
    for tc in ['nc', 'tc1', 'tc2', 'tc3']:
        compare_maps([scene, tc])




