#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os

data_dir = os.path.expanduser('~/eval/maps/results/')

cases_mse = [' mse_ae ',' mse_none ',' mse_gen ']
cases_ssim = [' ssim_ae ',' ssim_none ',' ssim_gen ']

save = True


# Generating scene-case-index dict
scenes = ['Aa','Ab','Ac','Ba','Bb','Bc','Ca','Cb','Cc','D','E','F']
cases = {'ab':['nc1','nc2','nc3','tc1','tc2','tc3'],'c':['nc1','nc2','nc3'],'def':['nc','tc1','tc2','tc3']}

ind = 0
rows = {scene: dict() for scene in scenes}
for i,scene in enumerate(scenes):
    if any(s in scene for s in scenes[:7]):
        for j,case in enumerate(cases['ab']):
            rows[scene].update({case:ind})
            ind +=1
    elif any(s in scene for s in scenes[7:9]):
        for j,case in enumerate(cases['c']):
            rows[scene].update({case:ind})
            ind +=1
    else:
        for j,case in enumerate(cases['def']):
            rows[scene].update({case:ind})
            ind +=1


##########################################################
## Plot results according to AE/noAE/gen
##########################################################

# ## #############
# ## Scenes D E F
# ## #############
df1 = pd.read_csv(data_dir + 'mse_ssim.csv', sep='\t', header = 0)
# data1,data2 = np.empty((1,3)),np.empty((1,3))
# for scene in scenes[9:]:
#     for case in rows[scene].keys():
#         # print(scene,case)
#         data1 = np.vstack((data1,[df1[cases_mse[0]].values[rows[scene][case]], df1[cases_mse[1]].values[rows[scene][case]], df1[cases_mse[2]].values[rows[scene][case]]]))
#         data2 = np.vstack((data2,[df1[cases_ssim[0]].values[rows[scene][case]], df1[cases_ssim[1]].values[rows[scene][case]], df1[cases_ssim[2]].values[rows[scene][case]]]))


# fig1, ax1 = plt.subplots()
# ax1.set_title('MSE of Scene D,E,F')
# ax1.boxplot(data1[1:,:])
# plt.setp(ax1,xticks=[1, 2, 3], xticklabels=cases_mse)


# fig2, ax2 = plt.subplots()
# ax2.set_title('SSIM of Scene D,E,F')
# ax2.boxplot(data2[1:,:])
# plt.setp(ax2,xticks=[1, 2, 3], xticklabels=cases_ssim)


## ###############
## Scenes A D E F
## ###############
data3,data4 = np.empty((1,3)),np.empty((1,3))
for scene in scenes[:3]+scenes[9:]:
    for case in rows[scene].keys():
        # print(scene,case)
        data3 = np.vstack((data3,[df1[cases_mse[0]].values[rows[scene][case]], df1[cases_mse[1]].values[rows[scene][case]], df1[cases_mse[2]].values[rows[scene][case]]]))
        data4 = np.vstack((data4,[df1[cases_ssim[0]].values[rows[scene][case]], df1[cases_ssim[1]].values[rows[scene][case]], df1[cases_ssim[2]].values[rows[scene][case]]]))


fig3, ax3 = plt.subplots()
ax3.set_title('MSE of Scene A,D,E,F')
ax3.boxplot(data3[1:,:])
plt.setp(ax3,xticks=[1, 2, 3], xticklabels=cases_mse)


fig4, ax4 = plt.subplots()
ax4.set_title('SSIM of Scene A,D,E,F')
ax4.boxplot(data4[1:,:])
plt.setp(ax4,xticks=[1, 2, 3], xticklabels=cases_ssim)

## ###############
## Scenes B C
## ###############
data5,data6 = np.empty((1,3)),np.empty((1,3))
for scene in scenes[3:9]:
    for case in rows[scene].keys():
        print(scene,case)
        data5 = np.vstack((data5,[df1[cases_mse[0]].values[rows[scene][case]], df1[cases_mse[1]].values[rows[scene][case]], df1[cases_mse[2]].values[rows[scene][case]]]))
        data6 = np.vstack((data6,[df1[cases_ssim[0]].values[rows[scene][case]], df1[cases_ssim[1]].values[rows[scene][case]], df1[cases_ssim[2]].values[rows[scene][case]]]))


fig5, ax5 = plt.subplots()
ax5.set_title('MSE of Scene B,C')
ax5.boxplot(data5[1:,:])
plt.setp(ax5,xticks=[1, 2, 3], xticklabels=cases_mse)


fig6, ax6 = plt.subplots()
ax6.set_title('SSIM of Scene B,C')
ax6.boxplot(data6[1:,:])
plt.setp(ax6,xticks=[1, 2, 3], xticklabels=cases_ssim)

##########################################################
## Plot per case
##########################################################

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color='red')

# For the differently coloured boxplots
pos = [[1,3.5,6],[1.5,4,6.5],[2,4.5,7],[2.5,5,7.5]]
colors = ['orange','blue','green','black']


# ## #############
# ## Scenes D E F
# ## #############

df5 = pd.read_csv(data_dir + 'mse_ssim.csv', sep='\t', header = 0)
# data_nc,data_tc1,data_tc2,data_tc3 = dict(),dict(),dict(),dict()
# data_nc['mse'],data_tc1['mse'],data_tc2['mse'],data_tc3['mse'] = np.empty((1,3)),np.empty((1,3)),np.empty((1,3)),np.empty((1,3))
# data_nc['ssim'],data_tc1['ssim'],data_tc2['ssim'],data_tc3['ssim'] = np.empty((1,3)),np.empty((1,3)),np.empty((1,3)),np.empty((1,3))

# for scene in scenes[9:]:
#     for case in rows[scene].keys():
#         if case == 'nc':
#             data_nc['mse'] = np.vstack((data_nc['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
#             data_nc['ssim'] = np.vstack((data_nc['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))
#         elif case == 'tc1':
#             data_tc1['mse'] = np.vstack((data_tc1['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
#             data_tc1['ssim'] = np.vstack((data_tc1['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))
#         elif case == 'tc2':
#             data_tc2['mse'] = np.vstack((data_tc2['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
#             data_tc2['ssim'] = np.vstack((data_tc2['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))
#         elif case == 'tc3':
#             data_tc3['mse'] = np.vstack((data_tc3['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
#             data_tc3['ssim'] = np.vstack((data_tc3['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))

# data_cases1 = [data_nc['mse'][1:,:],data_tc1['mse'][1:,:],data_tc2['mse'][1:,:],data_tc3['mse'][1:,:]]
# data_cases2 = [data_nc['ssim'][1:,:],data_tc1['ssim'][1:,:],data_tc2['ssim'][1:,:],data_tc3['ssim'][1:,:]]


# ## MSE

# # Plot 4 subplots
# fig5, ax5 = plt.subplots(nrows=round(len(cases['def'])/2),ncols=round(len(cases['def'])/2),figsize=(8,8))
# ax5 = ax5.ravel()
# for i,case in enumerate(cases['def']):
#     ax5[i].boxplot(data_cases1[i])
#     ax5[i].set_title(case)

# fig5.suptitle('MSE of Scene D,E,F')
# plt.setp(ax5,xticks=[1,2,3], xticklabels=cases_mse)
# plt.tight_layout(pad=1.5)
# plt.subplots_adjust(top=0.9)

# # Coloured boxplots
# fig51, ax51 = plt.subplots(figsize=(8,8))
# for i,case in enumerate(cases['def']):
#     bp = ax51.boxplot(data_cases1[i],positions=pos[i],widths=0.4)
#     set_box_color(bp,colors[i])

# for i,case in enumerate(cases['def']):
#     plt.plot([], c=colors[i], label=case) # draw temporary lines and use them to create a legend
# plt.legend()

# fig51.suptitle('MSE of Scene D,E,F')
# plt.setp(ax51,xticks=[1.75, 4.25, 6.75], xticklabels=cases_mse,xlim=[0.5,8])
# plt.tight_layout(pad=1.5)
# plt.subplots_adjust(top=0.9)


# ## SSIM

# # Plot different coloured boxplots
# fig6, ax6 = plt.subplots(figsize=(8,8))
# for i,case in enumerate(cases['def']):
#     bp = ax6.boxplot(data_cases2[i],positions=pos[i],widths=0.4)
#     set_box_color(bp,colors[i])

# for i,case in enumerate(cases['def']):
#     plt.plot([], c=colors[i], label=case) # draw temporary red and blue lines and use them to create a legend
# plt.legend()

# fig6.suptitle('SSIM of Scene D,E,F')
# plt.setp(ax6,xticks=[1.75, 4.25, 6.75], xticklabels=cases_mse,xlim=[0.5,8])
# plt.tight_layout(pad=1.5)
# plt.subplots_adjust(top=0.9)

## ###############
## Scenes A D E F
## ###############
print(scenes[:3]+scenes[9:])

data_nc,data_tc1,data_tc2,data_tc3 = dict(),dict(),dict(),dict()
data_nc['mse'],data_tc1['mse'],data_tc2['mse'],data_tc3['mse'] = np.empty((1,3)),np.empty((1,3)),np.empty((1,3)),np.empty((1,3))
data_nc['ssim'],data_tc1['ssim'],data_tc2['ssim'],data_tc3['ssim'] = np.empty((1,3)),np.empty((1,3)),np.empty((1,3)),np.empty((1,3))
for scene in scenes[:3]+scenes[9:]:
    for case in rows[scene].keys():
        if 'nc' in case:
            data_nc['mse'] = np.vstack((data_nc['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
            data_nc['ssim'] = np.vstack((data_nc['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))
        elif case == 'tc1':
            data_tc1['mse'] = np.vstack((data_tc1['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
            data_tc1['ssim'] = np.vstack((data_tc1['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))
        elif case == 'tc2':
            data_tc2['mse'] = np.vstack((data_tc2['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
            data_tc2['ssim'] = np.vstack((data_tc2['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))
        elif case == 'tc3':
            data_tc3['mse'] = np.vstack((data_tc3['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
            data_tc3['ssim'] = np.vstack((data_tc3['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))

data_cases3 = [data_nc['mse'][1:,:],data_tc1['mse'][1:,:],data_tc2['mse'][1:,:],data_tc3['mse'][1:,:]]
data_cases4 = [data_nc['ssim'][1:,:],data_tc1['ssim'][1:,:],data_tc2['ssim'][1:,:],data_tc3['ssim'][1:,:]]

# ## MSE

# Plot different coloured boxplots
fig7, ax7 = plt.subplots(figsize=(8,8))
for i,case in enumerate(cases['def']):
    bp = ax7.boxplot(data_cases3[i],positions=pos[i],widths=0.4)
    set_box_color(bp,colors[i])

for i,case in enumerate(cases['def']):
    plt.plot([], c=colors[i], label=case) # draw temporary lines and use them to create a legend
plt.legend()

fig7.suptitle('MSE of Scene A,D,E,F')
plt.setp(ax7,xticks=[1.75, 4.25, 6.75], xticklabels=cases_mse,xlim=[0.5,8])
plt.tight_layout(pad=1.5)
plt.subplots_adjust(top=0.9)


## SSIM

# Plot different coloured boxplots
fig8, ax8 = plt.subplots(figsize=(8,8))
for i,case in enumerate(cases['def']):
    bp = ax8.boxplot(data_cases4[i],positions=pos[i],widths=0.4)
    set_box_color(bp,colors[i])

for i,case in enumerate(cases['def']):
    plt.plot([], c=colors[i], label=case) # draw temporary red and blue lines and use them to create a legend
plt.legend()

fig8.suptitle('SSIM of Scene A,D,E,F')
plt.setp(ax8,xticks=[1.75, 4.25, 6.75], xticklabels=cases_mse,xlim=[0.5,8])
plt.tight_layout(pad=1.5)
plt.subplots_adjust(top=0.9)



## ###############
## Scenes B C
## ###############
print(scenes[3:9])

data_nc,data_tc1,data_tc2,data_tc3 = dict(),dict(),dict(),dict()
data_nc['mse'],data_tc1['mse'],data_tc2['mse'],data_tc3['mse'] = np.empty((1,3)),np.empty((1,3)),np.empty((1,3)),np.empty((1,3))
data_nc['ssim'],data_tc1['ssim'],data_tc2['ssim'],data_tc3['ssim'] = np.empty((1,3)),np.empty((1,3)),np.empty((1,3)),np.empty((1,3))
for scene in scenes[3:9]:
    for case in rows[scene].keys():
        if 'nc' in case:
            data_nc['mse'] = np.vstack((data_nc['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
            data_nc['ssim'] = np.vstack((data_nc['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))
        elif case == 'tc1':
            data_tc1['mse'] = np.vstack((data_tc1['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
            data_tc1['ssim'] = np.vstack((data_tc1['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))
        elif case == 'tc2':
            data_tc2['mse'] = np.vstack((data_tc2['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
            data_tc2['ssim'] = np.vstack((data_tc2['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))
        elif case == 'tc3':
            data_tc3['mse'] = np.vstack((data_tc3['mse'],[df5[cases_mse[0]].values[rows[scene][case]], df5[cases_mse[1]].values[rows[scene][case]], df5[cases_mse[2]].values[rows[scene][case]]]))
            data_tc3['ssim'] = np.vstack((data_tc3['ssim'],[df5[cases_ssim[0]].values[rows[scene][case]], df5[cases_ssim[1]].values[rows[scene][case]], df5[cases_ssim[2]].values[rows[scene][case]]]))

data_cases5 = [data_nc['mse'][1:,:],data_tc1['mse'][1:,:],data_tc2['mse'][1:,:],data_tc3['mse'][1:,:]]
data_cases6 = [data_nc['ssim'][1:,:],data_tc1['ssim'][1:,:],data_tc2['ssim'][1:,:],data_tc3['ssim'][1:,:]]

# ## MSE


# Plot different coloured boxplots
fig9, ax9 = plt.subplots(figsize=(8,8))
for i,case in enumerate(cases['def']):
    bp = ax9.boxplot(data_cases5[i],positions=pos[i],widths=0.4)
    set_box_color(bp,colors[i])

for i,case in enumerate(cases['def']):
    plt.plot([], c=colors[i], label=case) # draw temporary lines and use them to create a legend
plt.legend()

fig9.suptitle('MSE of Scene B,C')
plt.setp(ax9,xticks=[1.75, 4.25, 6.75], xticklabels=cases_mse,xlim=[0.5,8])
plt.tight_layout(pad=1.5)
plt.subplots_adjust(top=0.9)




## Plot SSIM for BC for each case
fig10, ax10 = plt.subplots(figsize=(8,8))
for i,case in enumerate(cases['def']):
    bp = ax10.boxplot(data_cases6[i],positions=pos[i],widths=0.4)
    set_box_color(bp,colors[i])

for i,case in enumerate(cases['def']):
    plt.plot([], c=colors[i], label=case) # draw temporary lines and use them to create a legend
plt.legend()

fig10.suptitle('SSIM of Scene B,C per case')
plt.setp(ax10,xticks=[1.75, 4.25, 6.75], xticklabels=cases_ssim,xlim=[0.5,8])
plt.tight_layout(pad=1.5)
plt.subplots_adjust(top=0.9)




## Plot mse for BC vs ADEF maps
mse_data = [data5[1:,:], data3[1:,:]]

fig11, ax11 = plt.subplots(figsize=(8,8))
for i, case in enumerate(['BC', 'ADEF']):
    bp = ax11.boxplot(mse_data[i],positions=pos[i],widths=0.4)
    set_box_color(bp,colors[i])

for i,case in enumerate(['BC', 'ADEF']):
    plt.plot([], c=colors[i], label=case) # draw temporary lines and use them to create a legend
plt.legend()

fig11.suptitle('MSE of all scenes')

plt.setp(ax11,xticks=[1.25, 3.75, 6.25], xticklabels=cases_mse, xlim=[0.5,8])
plt.tight_layout(pad=1.5)
plt.subplots_adjust(top=0.9)



## Plot ssim for BC vs ADEF maps
ssim_data = [data6[1:,:], data4[1:,:]]

fig12, ax12 = plt.subplots(figsize=(8,8))
for i, case in enumerate(['BC', 'ADEF']):
    bp = ax12.boxplot(ssim_data[i],positions=pos[i],widths=0.4)
    set_box_color(bp,colors[i])

for i,case in enumerate(['BC', 'ADEF']):
    plt.plot([], c=colors[i], label=case) # draw temporary lines and use them to create a legend
plt.legend()

fig12.suptitle('SSIM of all scenes')

plt.setp(ax12,xticks=[1.25, 3.75, 6.25], xticklabels=cases_ssim, xlim=[0.5,8])
plt.tight_layout(pad=1.5)
plt.subplots_adjust(top=0.9)

# ## SSIM

# # Plot different coloured boxplots
# fig10, ax10 = plt.subplots(figsize=(8,8))
# for i,case in enumerate(cases['def']):
#     bp = ax10.boxplot(data_cases6[i],positions=pos[i],widths=0.4)
#     set_box_color(bp,colors[i])

# for i,case in enumerate(cases['def']):
#     plt.plot([], c=colors[i], label=case) # draw temporary red and blue lines and use them to create a legend
# plt.legend()

# fig10.suptitle('SSIM of Scene B,C')
# plt.setp(ax10,xticks=[1.75, 4.25, 6.75], xticklabels=cases_mse,xlim=[0.5,8])
# plt.tight_layout(pad=1.5)
# plt.subplots_adjust(top=0.9)






# ax3.set_title('MSE of Scene A,D,E,F')
# ax4.set_title('SSIM of Scene A,D,E,F')
# ax5.set_title('MSE of Scene B,C')
# ax6.set_title('SSIM of Scene B,C')

# fig7.suptitle('MSE of Scene A,D,E,F per case')
# fig8.suptitle('SSIM of Scene A,D,E,F per case')
# fig9.suptitle('MSE of Scene B,C per case')
# fig10.suptitle('SSIM of Scene B,C per case')


# fig11.suptitle('MSE of all scenes')
# fig12.suptitle('SSIM of all scenes')

if save:
    fig3.savefig(data_dir + 'plots/mse_adef.png')
    fig4.savefig(data_dir + 'plots/ssim_adef.png')
    fig5.savefig(data_dir + 'plots/mse_bc.png')
    fig6.savefig(data_dir + 'plots/ssim_bc.png')

    fig7.savefig(data_dir + 'plots/mse_adef_per_case.png')
    fig8.savefig(data_dir + 'plots/ssim_adef_per_case.png')
    fig9.savefig(data_dir + 'plots/mse_bc_per_case.png')
    fig10.savefig(data_dir + 'plots/ssim_bc_per_case.png')

    fig11.savefig(data_dir + 'plots/mse_all_scenes.png')
    fig12.savefig(data_dir + 'plots/ssim_all_scenes.png')

else:
    plt.show()

