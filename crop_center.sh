#!/bin/sh 
 
############## Aanc1 ############# 
 
convert DF_maps/SceneAa_nc1.png -fuzz 1% -trim +repage DF_maps/SceneAa_nc1.png 
convert DF_maps/SceneAa_nc1.png -background black -gravity center -extent575x1111DF_maps/SceneAa_nc1.png 
############## Aanc2 ############# 
 
convert DF_maps/SceneAa_nc2.png -fuzz 1% -trim +repage DF_maps/SceneAa_nc2.png 
convert DF_maps/SceneAa_nc2.png -background black -gravity center -extent570x820DF_maps/SceneAa_nc2.png 
############## Aanc3 ############# 
 
convert DF_maps/SceneAa_nc3.png -fuzz 1% -trim +repage DF_maps/SceneAa_nc3.png 
convert DF_maps/SceneAa_nc3.png -background black -gravity center -extent711x990DF_maps/SceneAa_nc3.png 
############## Aatc1 ############# 
 
convert DF_maps/SceneAa_tc1.png -fuzz 1% -trim +repage DF_maps/SceneAa_tc1.png 
convert DF_maps/SceneAa_tc1.png -background black -gravity center -extent750x1299DF_maps/SceneAa_tc1.png 
############## Aatc2 ############# 
 
convert DF_maps/SceneAa_tc2.png -fuzz 1% -trim +repage DF_maps/SceneAa_tc2.png 
convert DF_maps/SceneAa_tc2.png -background black -gravity center -extent470x747DF_maps/SceneAa_tc2.png 
############## Aatc3 ############# 
 
convert DF_maps/SceneAa_tc3.png -fuzz 1% -trim +repage DF_maps/SceneAa_tc3.png 
convert DF_maps/SceneAa_tc3.png -background black -gravity center -extent1068x928DF_maps/SceneAa_tc3.png 
############## Abnc1 ############# 
 
convert DF_maps/SceneAb_nc1.png -fuzz 1% -trim +repage DF_maps/SceneAb_nc1.png 
convert DF_maps/SceneAb_nc1.png -background black -gravity center -extent591x640DF_maps/SceneAb_nc1.png 
############## Abnc2 ############# 
 
convert DF_maps/SceneAb_nc2.png -fuzz 1% -trim +repage DF_maps/SceneAb_nc2.png 
convert DF_maps/SceneAb_nc2.png -background black -gravity center -extent945x919DF_maps/SceneAb_nc2.png 
############## Abnc3 ############# 
 
convert DF_maps/SceneAb_nc3.png -fuzz 1% -trim +repage DF_maps/SceneAb_nc3.png 
convert DF_maps/SceneAb_nc3.png -background black -gravity center -extent483x765DF_maps/SceneAb_nc3.png 
############## Abtc1 ############# 
 
convert DF_maps/SceneAb_tc1.png -fuzz 1% -trim +repage DF_maps/SceneAb_tc1.png 
convert DF_maps/SceneAb_tc1.png -background black -gravity center -extent648x925DF_maps/SceneAb_tc1.png 
############## Abtc2 ############# 
 
convert DF_maps/SceneAb_tc2.png -fuzz 1% -trim +repage DF_maps/SceneAb_tc2.png 
convert DF_maps/SceneAb_tc2.png -background black -gravity center -extent895x965DF_maps/SceneAb_tc2.png 
############## Abtc3 ############# 
 
convert DF_maps/SceneAb_tc3.png -fuzz 1% -trim +repage DF_maps/SceneAb_tc3.png 
convert DF_maps/SceneAb_tc3.png -background black -gravity center -extent773x1019DF_maps/SceneAb_tc3.png 
############## Acnc1 ############# 
 
convert DF_maps/SceneAc_nc1.png -fuzz 1% -trim +repage DF_maps/SceneAc_nc1.png 
convert DF_maps/SceneAc_nc1.png -background black -gravity center -extent575x1111DF_maps/SceneAc_nc1.png 
############## Acnc2 ############# 
 
convert DF_maps/SceneAc_nc2.png -fuzz 1% -trim +repage DF_maps/SceneAc_nc2.png 
convert DF_maps/SceneAc_nc2.png -background black -gravity center -extent570x820DF_maps/SceneAc_nc2.png 
############## Acnc3 ############# 
 
convert DF_maps/SceneAc_nc3.png -fuzz 1% -trim +repage DF_maps/SceneAc_nc3.png 
convert DF_maps/SceneAc_nc3.png -background black -gravity center -extent711x990DF_maps/SceneAc_nc3.png 
############## Actc1 ############# 
 
convert DF_maps/SceneAc_tc1.png -fuzz 1% -trim +repage DF_maps/SceneAc_tc1.png 
convert DF_maps/SceneAc_tc1.png -background black -gravity center -extent750x1299DF_maps/SceneAc_tc1.png 
############## Actc2 ############# 
 
convert DF_maps/SceneAc_tc2.png -fuzz 1% -trim +repage DF_maps/SceneAc_tc2.png 
convert DF_maps/SceneAc_tc2.png -background black -gravity center -extent470x747DF_maps/SceneAc_tc2.png 
############## Actc3 ############# 
 
convert DF_maps/SceneAc_tc3.png -fuzz 1% -trim +repage DF_maps/SceneAc_tc3.png 
convert DF_maps/SceneAc_tc3.png -background black -gravity center -extent1068x928DF_maps/SceneAc_tc3.png 
############## Banc1 ############# 
 
convert DF_maps/SceneBa_nc1.png -fuzz 1% -trim +repage DF_maps/SceneBa_nc1.png 
convert DF_maps/SceneBa_nc1.png -background black -gravity center -extent591x640DF_maps/SceneBa_nc1.png 
############## Banc2 ############# 
 
convert DF_maps/SceneBa_nc2.png -fuzz 1% -trim +repage DF_maps/SceneBa_nc2.png 
convert DF_maps/SceneBa_nc2.png -background black -gravity center -extent945x919DF_maps/SceneBa_nc2.png 
############## Banc3 ############# 
 
convert DF_maps/SceneBa_nc3.png -fuzz 1% -trim +repage DF_maps/SceneBa_nc3.png 
convert DF_maps/SceneBa_nc3.png -background black -gravity center -extent483x765DF_maps/SceneBa_nc3.png 
############## Batc1 ############# 
 
convert DF_maps/SceneBa_tc1.png -fuzz 1% -trim +repage DF_maps/SceneBa_tc1.png 
convert DF_maps/SceneBa_tc1.png -background black -gravity center -extent648x925DF_maps/SceneBa_tc1.png 
############## Batc2 ############# 
 
convert DF_maps/SceneBa_tc2.png -fuzz 1% -trim +repage DF_maps/SceneBa_tc2.png 
convert DF_maps/SceneBa_tc2.png -background black -gravity center -extent895x965DF_maps/SceneBa_tc2.png 
############## Batc3 ############# 
 
convert DF_maps/SceneBa_tc3.png -fuzz 1% -trim +repage DF_maps/SceneBa_tc3.png 
convert DF_maps/SceneBa_tc3.png -background black -gravity center -extent773x1019DF_maps/SceneBa_tc3.png 
############## Bbnc1 ############# 
 
convert DF_maps/SceneBb_nc1.png -fuzz 1% -trim +repage DF_maps/SceneBb_nc1.png 
convert DF_maps/SceneBb_nc1.png -background black -gravity center -extent575x1111DF_maps/SceneBb_nc1.png 
############## Bbnc2 ############# 
 
convert DF_maps/SceneBb_nc2.png -fuzz 1% -trim +repage DF_maps/SceneBb_nc2.png 
convert DF_maps/SceneBb_nc2.png -background black -gravity center -extent570x820DF_maps/SceneBb_nc2.png 
############## Bbnc3 ############# 
 
convert DF_maps/SceneBb_nc3.png -fuzz 1% -trim +repage DF_maps/SceneBb_nc3.png 
convert DF_maps/SceneBb_nc3.png -background black -gravity center -extent711x990DF_maps/SceneBb_nc3.png 
############## Bbtc1 ############# 
 
convert DF_maps/SceneBb_tc1.png -fuzz 1% -trim +repage DF_maps/SceneBb_tc1.png 
convert DF_maps/SceneBb_tc1.png -background black -gravity center -extent750x1299DF_maps/SceneBb_tc1.png 
############## Bbtc2 ############# 
 
convert DF_maps/SceneBb_tc2.png -fuzz 1% -trim +repage DF_maps/SceneBb_tc2.png 
convert DF_maps/SceneBb_tc2.png -background black -gravity center -extent470x747DF_maps/SceneBb_tc2.png 
############## Bbtc3 ############# 
 
convert DF_maps/SceneBb_tc3.png -fuzz 1% -trim +repage DF_maps/SceneBb_tc3.png 
convert DF_maps/SceneBb_tc3.png -background black -gravity center -extent1068x928DF_maps/SceneBb_tc3.png 
############## Bcnc1 ############# 
 
convert DF_maps/SceneBc_nc1.png -fuzz 1% -trim +repage DF_maps/SceneBc_nc1.png 
convert DF_maps/SceneBc_nc1.png -background black -gravity center -extent591x640DF_maps/SceneBc_nc1.png 
############## Bcnc2 ############# 
 
convert DF_maps/SceneBc_nc2.png -fuzz 1% -trim +repage DF_maps/SceneBc_nc2.png 
convert DF_maps/SceneBc_nc2.png -background black -gravity center -extent945x919DF_maps/SceneBc_nc2.png 
############## Bcnc3 ############# 
 
convert DF_maps/SceneBc_nc3.png -fuzz 1% -trim +repage DF_maps/SceneBc_nc3.png 
convert DF_maps/SceneBc_nc3.png -background black -gravity center -extent483x765DF_maps/SceneBc_nc3.png 
############## Bctc1 ############# 
 
convert DF_maps/SceneBc_tc1.png -fuzz 1% -trim +repage DF_maps/SceneBc_tc1.png 
convert DF_maps/SceneBc_tc1.png -background black -gravity center -extent648x925DF_maps/SceneBc_tc1.png 
############## Bctc2 ############# 
 
convert DF_maps/SceneBc_tc2.png -fuzz 1% -trim +repage DF_maps/SceneBc_tc2.png 
convert DF_maps/SceneBc_tc2.png -background black -gravity center -extent895x965DF_maps/SceneBc_tc2.png 
############## Bctc3 ############# 
 
convert DF_maps/SceneBc_tc3.png -fuzz 1% -trim +repage DF_maps/SceneBc_tc3.png 
convert DF_maps/SceneBc_tc3.png -background black -gravity center -extent773x1019DF_maps/SceneBc_tc3.png 
############## Canc1 ############# 
 
convert DF_maps/SceneCa_nc1.png -fuzz 1% -trim +repage DF_maps/SceneCa_nc1.png 
convert DF_maps/SceneCa_nc1.png -background black -gravity center -extent575x1111DF_maps/SceneCa_nc1.png 
############## Canc2 ############# 
 
convert DF_maps/SceneCa_nc2.png -fuzz 1% -trim +repage DF_maps/SceneCa_nc2.png 
convert DF_maps/SceneCa_nc2.png -background black -gravity center -extent570x820DF_maps/SceneCa_nc2.png 
############## Canc3 ############# 
 
convert DF_maps/SceneCa_nc3.png -fuzz 1% -trim +repage DF_maps/SceneCa_nc3.png 
convert DF_maps/SceneCa_nc3.png -background black -gravity center -extent711x990DF_maps/SceneCa_nc3.png 
############## Catc1 ############# 
 
convert DF_maps/SceneCa_tc1.png -fuzz 1% -trim +repage DF_maps/SceneCa_tc1.png 
convert DF_maps/SceneCa_tc1.png -background black -gravity center -extent750x1299DF_maps/SceneCa_tc1.png 
############## Catc2 ############# 
 
convert DF_maps/SceneCa_tc2.png -fuzz 1% -trim +repage DF_maps/SceneCa_tc2.png 
convert DF_maps/SceneCa_tc2.png -background black -gravity center -extent470x747DF_maps/SceneCa_tc2.png 
############## Catc3 ############# 
 
convert DF_maps/SceneCa_tc3.png -fuzz 1% -trim +repage DF_maps/SceneCa_tc3.png 
convert DF_maps/SceneCa_tc3.png -background black -gravity center -extent1068x928DF_maps/SceneCa_tc3.png 
############## Cbnc1 ############# 
 
convert DF_maps/SceneCb_nc1.png -fuzz 1% -trim +repage DF_maps/SceneCb_nc1.png 
convert DF_maps/SceneCb_nc1.png -background black -gravity center -extent591x640DF_maps/SceneCb_nc1.png 
############## Cbnc2 ############# 
 
convert DF_maps/SceneCb_nc2.png -fuzz 1% -trim +repage DF_maps/SceneCb_nc2.png 
convert DF_maps/SceneCb_nc2.png -background black -gravity center -extent945x919DF_maps/SceneCb_nc2.png 
############## Cbnc3 ############# 
 
convert DF_maps/SceneCb_nc3.png -fuzz 1% -trim +repage DF_maps/SceneCb_nc3.png 
convert DF_maps/SceneCb_nc3.png -background black -gravity center -extent483x765DF_maps/SceneCb_nc3.png 
############## Ccnc1 ############# 
 
convert DF_maps/SceneCc_nc1.png -fuzz 1% -trim +repage DF_maps/SceneCc_nc1.png 
convert DF_maps/SceneCc_nc1.png -background black -gravity center -extent648x925DF_maps/SceneCc_nc1.png 
############## Ccnc2 ############# 
 
convert DF_maps/SceneCc_nc2.png -fuzz 1% -trim +repage DF_maps/SceneCc_nc2.png 
convert DF_maps/SceneCc_nc2.png -background black -gravity center -extent895x965DF_maps/SceneCc_nc2.png 
############## Ccnc3 ############# 
 
convert DF_maps/SceneCc_nc3.png -fuzz 1% -trim +repage DF_maps/SceneCc_nc3.png 
convert DF_maps/SceneCc_nc3.png -background black -gravity center -extent773x1019DF_maps/SceneCc_nc3.png 
