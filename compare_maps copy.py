#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os
import math
# import cv2
from skimage.io import imread
from skimage import data, img_as_float
from skimage.metrics import structural_similarity as ssim
from skimage.metrics import mean_squared_error as mse
import pathlib
from skimage.color import rgb2gray, rgba2rgb
from scipy import stats

import cv2


from sklearn.cluster import KMeans




MAX_FEATURES = 30000
GOOD_MATCH_PERCENT = 0.02


def compare_images(imageA, imageB, imageC, title):
    # compute the mean squared error and structural similarity
    # index for the images
    m = mse(imageA, imageB)
    s = ssim(imageA, imageB)

    m2 = mse(imageA, imageC)
    s2 = ssim(imageA, imageC)

    m3 = mse(imageB, imageC)
    s3 = ssim(imageB, imageC)
    # setup the figure
    fig = plt.figure(title)
    plt.suptitle("With AE - MSE: %.2f, SSIM: %.2f \n Without AE - MSE: %.2f, SSIM: %.2f \n Between generated - MSE: %.2f, SSIM: %.2f " % (m, s, m2, s2, m3, s3))
    # show first image
    ax = fig.add_subplot(1, 3, 1)
    plt.imshow(imageA, cmap=plt.cm.gray)
    plt.title('Ground truth')
    plt.axis("off")
    # show the second image
    ax = fig.add_subplot(1, 3, 2)
    plt.imshow(imageB, cmap=plt.cm.gray)
    plt.title('With AE')
    plt.axis("off")

    ax = fig.add_subplot(1, 3, 3)
    plt.imshow(imageC, cmap=plt.cm.gray)
    plt.title('Without AE')
    plt.axis("off")
    # show the images
    # plt.show()
    if save:
        fig.savefig(save_dir+title+'.png')
        plt.close(fig)
    else:
        plt.show()

    return [[m, s], [m2, s2], [m3, s3]]

def get_trimmed_mean(arr):

    print('orig', arr)
    arr = np.sort(arr)
    print('sorted:', arr)
    upper_quartile = np.percentile(arr, 75)
    lower_quartile = np.percentile(arr, 25)

    print('quantiles: ', lower_quartile, upper_quartile)

    resultList = []
    for y in arr:
        if y >= lower_quartile and y <= upper_quartile:
            resultList.append(y)

    print('resultlist',resultList)

    return np.mean(resultList)

def alignImages(im1, im2):
    # Convert images to grayscale
    im1Gray = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
    im2Gray = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)

    # Detect ORB features and compute descriptors.
    orb = cv2.SIFT_create()
    keypoints1, descriptors1 = orb.detectAndCompute(im1Gray, None)
    keypoints2, descriptors2 = orb.detectAndCompute(im2Gray, None)

    # FLANN parameters
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=100)   # or pass empty dictionary

    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(descriptors1, descriptors2, k=2)
    # Need to draw only good matches, so create a mask
    matchesMask = [[0, 0] for i in range(len(matches))]

    # ratio test as per Lowe's paper
    for i, (m, n) in enumerate(matches):
        if m.distance < 0.7*n.distance:
            matchesMask[i] = [1, 0]
       

    # end of for
    draw_params = dict(matchColor=(0, 255, 0),
                           singlePointColor=(255, 0, 0),
                           matchesMask=matchesMask,
                           flags=cv2.DrawMatchesFlags_DEFAULT)

    # Draw top matches
    imMatches = cv2.drawMatchesKnn(
        im1Gray, keypoints1, im2Gray, keypoints2, matches, None, **draw_params)

    cv2.imwrite("matches.jpg", imMatches)

    points1 = []
    points2 = []

    for i, match in enumerate(matches):
        p1 = keypoints1[match[0].queryIdx].pt
        p2 = keypoints2[match[0].trainIdx].pt
        if matchesMask[i][0] == 1:
            points1.append(p1)
            points2.append(p2)

    # Remove duplicates
    comb_points = []
    for i in range(len(points1)):
        comb_points.append((points1[i][0], points1[i][1], points2[i][0], points2[i][1]))
       

    comb_points = list(set(comb_points))

    print('------------------')

    points1 = []
    points2 = []
    for i in comb_points:
        points1.append(i[0:2])
        points2.append(i[2:])
        
           


    # Find translation
    points1 = np.float32(points1)
    points2 = np.float32(points2)
    
    diff_x = []
    diff_y = []

    for i in range(len(points1)):
        diff_x.append(points2[i][0] - points1[i][0])
        diff_y.append(points2[i][1] - points1[i][1])


    mean_x = get_trimmed_mean(diff_x)
    mean_y = get_trimmed_mean(diff_y)

    print('means', mean_x, mean_y)


    # # Use Translation
    height, width, channels = im2.shape
    M = np.float32([[1,0,mean_y],[0,1,mean_x]])
    im1Reg = cv2.warpAffine(im1, M, (width, height))

    return im1Reg


map_dir = os.path.expanduser('~/eval/maps')


save_dir = os.path.expanduser('~/eval/maps/compared_maps/altered_aligned/')
save = True
pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True)

testdir = os.path.expanduser('~/Downloads/maps/test')


# Read reference image
# refFilename = map_dir + "/ground_truths/SceneA_nc.png"
# imFilename =  map_dir + "/default_threshold/limB_AE_Aa_nc1.pgm"

refFilename = testdir + "/limB_AE_Aa_nc1.png"
imFilename = testdir + "/SceneA_nc.png"
imReference = cv2.imread(refFilename, cv2.IMREAD_COLOR)
im = cv2.imread(imFilename, cv2.IMREAD_COLOR)

print(imReference.shape, im.shape)
# print(imReference.shape[0]/im.shape[0], imReference.shape[1]/im.shape[1])

# scale_percent = max([imReference.shape[0]/im.shape[0],
#                     imReference.shape[1]/im.shape[1]])
# scale_percent = math.ceil(scale_percent*100)/100
# print(scale_percent)

# width = int(im.shape[1] * scale_percent)
# height = int(im.shape[0] * scale_percent)
# dim = (width, height)

# # resize image
# resized = cv2.resize(im, dim, interpolation=cv2.INTER_AREA)

# print('Resized Dimensions : ', resized.shape)

# cv2.imshow("Resized image", resized)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

########################33
# Registered image will be resotred in imReg.
# The estimated homography will be stored in h.
imReg = alignImages(im, imReference)

# # # Write aligned image to disk.
outFilename = "aligned.png"
cv2.imwrite(outFilename, imReg)


# results = compare_images(ground_truth, map_with_ae, map_without_ae, scene + '_' + tc + '_aligned')
#########################

# f= open(map_dir + "/results/mse_ssim_altered.csv","w+")
# f.write('case \t mse_ae \t mse_none \t mse_gen \t ssim_ae \t ssim_none \t ssim_gen \n')
# f.close()

# for scene in ['D', 'E', 'F']:
# 	for tc in ['nc', 'tc1', 'tc2', 'tc3']:


# 		truth = map_dir + '/DEF_maps/ground_truths/Scene' + scene + '_' + tc + '.png'
# 		with_ae = map_dir + '/DEF_maps/altered_thresh/limB_AE_' + scene + '_' + tc + '.png'
# 		without_ae = map_dir + '/DEF_maps/altered_thresh/no_AE_' + scene + '_' + tc + '.png'

# 		ground_truth = imread(truth)
# 		map_with_ae = imread(with_ae)
# 		map_without_ae = imread(without_ae)

# 		# print(scene, tc, ground_truth.shape, map_with_ae.shape, map_without_ae.shape)


# 		results = compare_images(ground_truth, map_with_ae, map_without_ae, scene + '_' + tc + '_aligned')

# 		f= open(map_dir + "/results/mse_ssim_altered.csv","a+")
# 		#        ('case \t          mse_ae \t                   mse_none \t                 mse_gen \t                   ssim_ae \t                   ssim_none \t               ssim_gen \n')
# 		f.write(scene + tc + '\t' + str(results[0][0]) + '\t' + str(results[1][0]) + '\t' + str(results[2][0]) + '\t'  + str(results[0][1]) + '\t' + str(results[1][1]) + '\t' + str(results[2][1]) + '\n')
# 		f.close()
