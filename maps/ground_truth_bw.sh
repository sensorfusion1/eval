#!/bin/sh


for scene in 'A' 'B' 'C'
do
for case in 'nc' 'tc1' 'tc2' 'tc3'
do
echo "----- $scene $case ----"
convert "ABC_maps/ground_truths/Scene$scene""_$case"".png" -alpha remove -alpha off "ABC_maps/ground_truths/Scene$scene""_$case"".png"   
convert "ABC_maps/ground_truths/Scene$scene""_$case"".png" -threshold 90% "ABC_maps/ground_truths/Scene$scene""_$case"".png"
done
done



