#!/bin/sh

DEF_maps

for scene in 'D' 'E' 'F'
do
for case in 'nc' 'tc1' 'tc2' 'tc3'
do
echo "----- $scene $case ----"
convert "DEF_maps/default_thresh/limB_AE_$scene""_$case"".png" -fuzz 1% -trim +repage "DEF_maps/default_thresh/limB_AE_$scene""_$case"".png"
convert "DEF_maps/default_thresh/no_AE_$scene""_$case"".png" -fuzz 1% -trim +repage "DEF_maps/default_thresh/no_AE_$scene""_$case"".png"

identify -format '%w, %h\n' "DEF_maps/default_thresh/limB_AE_$scene""_$case"".png"
identify -format '%w, %h\n' "DEF_maps/default_thresh/no_AE_$scene""_$case"".png"
done
done
