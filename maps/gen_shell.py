



img_sizes = ['575x1111', '570x820', '711x990', '750x1299', '470x747', '1068x928', '591x640', '945x919', '483x765', '648x925', '895x965', '773x1019']
img_sizes = img_sizes * 4


cases = []

for i in ['Aa', 'Ab', 'Ac', 'Ba', 'Bb', 'Bc', 'Ca']:
    for j in ['nc1', 'nc2', 'nc3', 'tc1', 'tc2', 'tc3']:
        cases.append([i, j])

for i in ['Cb', 'Cc']:
    for j in ['nc1', 'nc2', 'nc3']:
        cases.append([i, j])
print(len(cases))

f = open("crop_center.sh", "w+")
f.write("#!/bin/sh \n \n")
f.close()


f = open("crop_center.sh", "a+")

for i in range(len(cases)):
    f.write("############## " + cases[i][0] + cases[i][1] + " ############# \n \n")

    f.write("convert ABC_maps/default_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -fuzz 1% -trim +repage ABC_maps/default_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n")
    f.write("convert ABC_maps/default_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -background black -gravity center -extent" + img_sizes[i] +  "ABC_maps/default_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n")

    f.write("convert ABC_maps/default_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -fuzz 1% -trim +repage ABC_maps/default_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n")
    f.write("convert ABC_maps/default_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -background black -gravity center -extent" + img_sizes[i] +  "ABC_maps/default_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n")




f.close()
        # convert SceneA_nc.png -fuzz 1% -trim +repage SceneA_nc.png
        # convert SceneA_nc.png -background black -gravity center -extent 518x481 SceneA_nc.png 





# f = open("shell.txt", "a+")

# for i in range(len(img_sizes)):
    
#     f.write("############## " + cases[i][0] + cases[i][1] + " ############# \n \n")

#     f.write("# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension \n")
#     f.write("convert DF_maps/Scene" + cases[i][0] + "_" + cases[i][1] + ".png -alpha remove -alpha off DF_maps/Scene" + cases[i][0] + "_" + cases[i][1] + ".png \n")
#     f.write("convert DF_maps/Scene" + cases[i][0] + "_" + cases[i][1] + ".png -resize "+ img_sizes[i]+ " -background black -gravity center -extent "+ img_sizes[i]+ " DF_maps/Scene" + cases[i][0] + "_" + cases[i][1] + ".png \n")
#     #f.write("convert DF_maps/Scene" + cases[i][0] + "_" + cases[i][1] + ".png -threshold 90% DF_maps/Scene" + cases[i][0] + "_" + cases[i][1] + ".png \n \n")


#     f.write("# altered thresh maps, go from greyscale to black and white \n")
#     f.write("convert DF_maps/altered_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -threshold 90% DF_maps/altered_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n")
#     f.write("convert DF_maps/altered_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -threshold 90% DF_maps/altered_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n \n")


#     f.write("# default thresh maps, go from greyscale to black and white \n")
#     f.write("convert DF_maps/default_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -threshold 90% DF_maps/default_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n")
#     f.write("convert DF_maps/default_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -threshold 90% DF_maps/default_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n \n")


#     f.write("# Extend the generated maps to fit largest image size of the four (from sizes of maps) \n")
#     f.write("convert DF_maps/altered_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -gravity center -background black -extent "+ img_sizes[i]+ " DF_maps/altered_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n")
#     f.write("convert DF_maps/altered_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -gravity center -background black -extent "+ img_sizes[i]+ " DF_maps/altered_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n")
#     f.write("convert DF_maps/default_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -gravity center -background black -extent "+ img_sizes[i]+ " DF_maps/default_thresh/limB_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n")
#     f.write("convert DF_maps/default_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png -gravity center -background black -extent "+ img_sizes[i]+ " DF_maps/default_thresh/no_AE_" + cases[i][0] + "_" + cases[i][1] + ".png \n \n \n")



# f.close()