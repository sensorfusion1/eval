#!/bin/sh
############## Dnc ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneD_nc.png -alpha remove -alpha off DF_maps/SceneD_nc.png 
convert DF_maps/SceneD_nc.png -resize 575x1111 -background black -gravity center -extent 575x1111 DF_maps/SceneD_nc.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_D_nc.png -threshold 90% DF_maps/altered_thresh/limB_AE_D_nc.png 
convert DF_maps/altered_thresh/no_AE_D_nc.png -threshold 90% DF_maps/altered_thresh/no_AE_D_nc.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_D_nc.png -threshold 90% DF_maps/default_thresh/limB_AE_D_nc.png 
convert DF_maps/default_thresh/no_AE_D_nc.png -threshold 90% DF_maps/default_thresh/no_AE_D_nc.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_D_nc.png -gravity center -background black -extent 575x1111 DF_maps/altered_thresh/limB_AE_D_nc.png 
convert DF_maps/altered_thresh/no_AE_D_nc.png -gravity center -background black -extent 575x1111 DF_maps/altered_thresh/no_AE_D_nc.png 
convert DF_maps/default_thresh/limB_AE_D_nc.png -gravity center -background black -extent 575x1111 DF_maps/default_thresh/limB_AE_D_nc.png 
convert DF_maps/default_thresh/no_AE_D_nc.png -gravity center -background black -extent 575x1111 DF_maps/default_thresh/no_AE_D_nc.png 
 
 
############## Dtc1 ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneD_tc1.png -alpha remove -alpha off DF_maps/SceneD_tc1.png 
convert DF_maps/SceneD_tc1.png -resize 570x820 -background black -gravity center -extent 570x820 DF_maps/SceneD_tc1.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_D_tc1.png -threshold 90% DF_maps/altered_thresh/limB_AE_D_tc1.png 
convert DF_maps/altered_thresh/no_AE_D_tc1.png -threshold 90% DF_maps/altered_thresh/no_AE_D_tc1.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_D_tc1.png -threshold 90% DF_maps/default_thresh/limB_AE_D_tc1.png 
convert DF_maps/default_thresh/no_AE_D_tc1.png -threshold 90% DF_maps/default_thresh/no_AE_D_tc1.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_D_tc1.png -gravity center -background black -extent 570x820 DF_maps/altered_thresh/limB_AE_D_tc1.png 
convert DF_maps/altered_thresh/no_AE_D_tc1.png -gravity center -background black -extent 570x820 DF_maps/altered_thresh/no_AE_D_tc1.png 
convert DF_maps/default_thresh/limB_AE_D_tc1.png -gravity center -background black -extent 570x820 DF_maps/default_thresh/limB_AE_D_tc1.png 
convert DF_maps/default_thresh/no_AE_D_tc1.png -gravity center -background black -extent 570x820 DF_maps/default_thresh/no_AE_D_tc1.png 
 
 
############## Dtc2 ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneD_tc2.png -alpha remove -alpha off DF_maps/SceneD_tc2.png 
convert DF_maps/SceneD_tc2.png -resize 711x990 -background black -gravity center -extent 711x990 DF_maps/SceneD_tc2.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_D_tc2.png -threshold 90% DF_maps/altered_thresh/limB_AE_D_tc2.png 
convert DF_maps/altered_thresh/no_AE_D_tc2.png -threshold 90% DF_maps/altered_thresh/no_AE_D_tc2.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_D_tc2.png -threshold 90% DF_maps/default_thresh/limB_AE_D_tc2.png 
convert DF_maps/default_thresh/no_AE_D_tc2.png -threshold 90% DF_maps/default_thresh/no_AE_D_tc2.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_D_tc2.png -gravity center -background black -extent 711x990 DF_maps/altered_thresh/limB_AE_D_tc2.png 
convert DF_maps/altered_thresh/no_AE_D_tc2.png -gravity center -background black -extent 711x990 DF_maps/altered_thresh/no_AE_D_tc2.png 
convert DF_maps/default_thresh/limB_AE_D_tc2.png -gravity center -background black -extent 711x990 DF_maps/default_thresh/limB_AE_D_tc2.png 
convert DF_maps/default_thresh/no_AE_D_tc2.png -gravity center -background black -extent 711x990 DF_maps/default_thresh/no_AE_D_tc2.png 
 
 
############## Dtc3 ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneD_tc3.png -alpha remove -alpha off DF_maps/SceneD_tc3.png 
convert DF_maps/SceneD_tc3.png -resize 750x1299 -background black -gravity center -extent 750x1299 DF_maps/SceneD_tc3.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_D_tc3.png -threshold 90% DF_maps/altered_thresh/limB_AE_D_tc3.png 
convert DF_maps/altered_thresh/no_AE_D_tc3.png -threshold 90% DF_maps/altered_thresh/no_AE_D_tc3.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_D_tc3.png -threshold 90% DF_maps/default_thresh/limB_AE_D_tc3.png 
convert DF_maps/default_thresh/no_AE_D_tc3.png -threshold 90% DF_maps/default_thresh/no_AE_D_tc3.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_D_tc3.png -gravity center -background black -extent 750x1299 DF_maps/altered_thresh/limB_AE_D_tc3.png 
convert DF_maps/altered_thresh/no_AE_D_tc3.png -gravity center -background black -extent 750x1299 DF_maps/altered_thresh/no_AE_D_tc3.png 
convert DF_maps/default_thresh/limB_AE_D_tc3.png -gravity center -background black -extent 750x1299 DF_maps/default_thresh/limB_AE_D_tc3.png 
convert DF_maps/default_thresh/no_AE_D_tc3.png -gravity center -background black -extent 750x1299 DF_maps/default_thresh/no_AE_D_tc3.png 
 
 
############## Enc ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneE_nc.png -alpha remove -alpha off DF_maps/SceneE_nc.png 
convert DF_maps/SceneE_nc.png -resize 470x747 -background black -gravity center -extent 470x747 DF_maps/SceneE_nc.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_E_nc.png -threshold 90% DF_maps/altered_thresh/limB_AE_E_nc.png 
convert DF_maps/altered_thresh/no_AE_E_nc.png -threshold 90% DF_maps/altered_thresh/no_AE_E_nc.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_E_nc.png -threshold 90% DF_maps/default_thresh/limB_AE_E_nc.png 
convert DF_maps/default_thresh/no_AE_E_nc.png -threshold 90% DF_maps/default_thresh/no_AE_E_nc.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_E_nc.png -gravity center -background black -extent 470x747 DF_maps/altered_thresh/limB_AE_E_nc.png 
convert DF_maps/altered_thresh/no_AE_E_nc.png -gravity center -background black -extent 470x747 DF_maps/altered_thresh/no_AE_E_nc.png 
convert DF_maps/default_thresh/limB_AE_E_nc.png -gravity center -background black -extent 470x747 DF_maps/default_thresh/limB_AE_E_nc.png 
convert DF_maps/default_thresh/no_AE_E_nc.png -gravity center -background black -extent 470x747 DF_maps/default_thresh/no_AE_E_nc.png 
 
 
############## Etc1 ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneE_tc1.png -alpha remove -alpha off DF_maps/SceneE_tc1.png 
convert DF_maps/SceneE_tc1.png -resize 1068x928 -background black -gravity center -extent 1068x928 DF_maps/SceneE_tc1.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_E_tc1.png -threshold 90% DF_maps/altered_thresh/limB_AE_E_tc1.png 
convert DF_maps/altered_thresh/no_AE_E_tc1.png -threshold 90% DF_maps/altered_thresh/no_AE_E_tc1.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_E_tc1.png -threshold 90% DF_maps/default_thresh/limB_AE_E_tc1.png 
convert DF_maps/default_thresh/no_AE_E_tc1.png -threshold 90% DF_maps/default_thresh/no_AE_E_tc1.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_E_tc1.png -gravity center -background black -extent 1068x928 DF_maps/altered_thresh/limB_AE_E_tc1.png 
convert DF_maps/altered_thresh/no_AE_E_tc1.png -gravity center -background black -extent 1068x928 DF_maps/altered_thresh/no_AE_E_tc1.png 
convert DF_maps/default_thresh/limB_AE_E_tc1.png -gravity center -background black -extent 1068x928 DF_maps/default_thresh/limB_AE_E_tc1.png 
convert DF_maps/default_thresh/no_AE_E_tc1.png -gravity center -background black -extent 1068x928 DF_maps/default_thresh/no_AE_E_tc1.png 
 
 
############## Etc2 ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneE_tc2.png -alpha remove -alpha off DF_maps/SceneE_tc2.png 
convert DF_maps/SceneE_tc2.png -resize 591x640 -background black -gravity center -extent 591x640 DF_maps/SceneE_tc2.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_E_tc2.png -threshold 90% DF_maps/altered_thresh/limB_AE_E_tc2.png 
convert DF_maps/altered_thresh/no_AE_E_tc2.png -threshold 90% DF_maps/altered_thresh/no_AE_E_tc2.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_E_tc2.png -threshold 90% DF_maps/default_thresh/limB_AE_E_tc2.png 
convert DF_maps/default_thresh/no_AE_E_tc2.png -threshold 90% DF_maps/default_thresh/no_AE_E_tc2.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_E_tc2.png -gravity center -background black -extent 591x640 DF_maps/altered_thresh/limB_AE_E_tc2.png 
convert DF_maps/altered_thresh/no_AE_E_tc2.png -gravity center -background black -extent 591x640 DF_maps/altered_thresh/no_AE_E_tc2.png 
convert DF_maps/default_thresh/limB_AE_E_tc2.png -gravity center -background black -extent 591x640 DF_maps/default_thresh/limB_AE_E_tc2.png 
convert DF_maps/default_thresh/no_AE_E_tc2.png -gravity center -background black -extent 591x640 DF_maps/default_thresh/no_AE_E_tc2.png 
 
 
############## Etc3 ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneE_tc3.png -alpha remove -alpha off DF_maps/SceneE_tc3.png 
convert DF_maps/SceneE_tc3.png -resize 945x919 -background black -gravity center -extent 945x919 DF_maps/SceneE_tc3.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_E_tc3.png -threshold 90% DF_maps/altered_thresh/limB_AE_E_tc3.png 
convert DF_maps/altered_thresh/no_AE_E_tc3.png -threshold 90% DF_maps/altered_thresh/no_AE_E_tc3.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_E_tc3.png -threshold 90% DF_maps/default_thresh/limB_AE_E_tc3.png 
convert DF_maps/default_thresh/no_AE_E_tc3.png -threshold 90% DF_maps/default_thresh/no_AE_E_tc3.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_E_tc3.png -gravity center -background black -extent 945x919 DF_maps/altered_thresh/limB_AE_E_tc3.png 
convert DF_maps/altered_thresh/no_AE_E_tc3.png -gravity center -background black -extent 945x919 DF_maps/altered_thresh/no_AE_E_tc3.png 
convert DF_maps/default_thresh/limB_AE_E_tc3.png -gravity center -background black -extent 945x919 DF_maps/default_thresh/limB_AE_E_tc3.png 
convert DF_maps/default_thresh/no_AE_E_tc3.png -gravity center -background black -extent 945x919 DF_maps/default_thresh/no_AE_E_tc3.png 
 
 
############## Fnc ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneF_nc.png -alpha remove -alpha off DF_maps/SceneF_nc.png 
convert DF_maps/SceneF_nc.png -resize 483x765 -background black -gravity center -extent 483x765 DF_maps/SceneF_nc.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_F_nc.png -threshold 90% DF_maps/altered_thresh/limB_AE_F_nc.png 
convert DF_maps/altered_thresh/no_AE_F_nc.png -threshold 90% DF_maps/altered_thresh/no_AE_F_nc.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_F_nc.png -threshold 90% DF_maps/default_thresh/limB_AE_F_nc.png 
convert DF_maps/default_thresh/no_AE_F_nc.png -threshold 90% DF_maps/default_thresh/no_AE_F_nc.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_F_nc.png -gravity center -background black -extent 483x765 DF_maps/altered_thresh/limB_AE_F_nc.png 
convert DF_maps/altered_thresh/no_AE_F_nc.png -gravity center -background black -extent 483x765 DF_maps/altered_thresh/no_AE_F_nc.png 
convert DF_maps/default_thresh/limB_AE_F_nc.png -gravity center -background black -extent 483x765 DF_maps/default_thresh/limB_AE_F_nc.png 
convert DF_maps/default_thresh/no_AE_F_nc.png -gravity center -background black -extent 483x765 DF_maps/default_thresh/no_AE_F_nc.png 
 
 
############## Ftc1 ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneF_tc1.png -alpha remove -alpha off DF_maps/SceneF_tc1.png 
convert DF_maps/SceneF_tc1.png -resize 648x925 -background black -gravity center -extent 648x925 DF_maps/SceneF_tc1.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_F_tc1.png -threshold 90% DF_maps/altered_thresh/limB_AE_F_tc1.png 
convert DF_maps/altered_thresh/no_AE_F_tc1.png -threshold 90% DF_maps/altered_thresh/no_AE_F_tc1.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_F_tc1.png -threshold 90% DF_maps/default_thresh/limB_AE_F_tc1.png 
convert DF_maps/default_thresh/no_AE_F_tc1.png -threshold 90% DF_maps/default_thresh/no_AE_F_tc1.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_F_tc1.png -gravity center -background black -extent 648x925 DF_maps/altered_thresh/limB_AE_F_tc1.png 
convert DF_maps/altered_thresh/no_AE_F_tc1.png -gravity center -background black -extent 648x925 DF_maps/altered_thresh/no_AE_F_tc1.png 
convert DF_maps/default_thresh/limB_AE_F_tc1.png -gravity center -background black -extent 648x925 DF_maps/default_thresh/limB_AE_F_tc1.png 
convert DF_maps/default_thresh/no_AE_F_tc1.png -gravity center -background black -extent 648x925 DF_maps/default_thresh/no_AE_F_tc1.png 
 
 
############## Ftc2 ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneF_tc2.png -alpha remove -alpha off DF_maps/SceneF_tc2.png 
convert DF_maps/SceneF_tc2.png -resize 895x965 -background black -gravity center -extent 895x965 DF_maps/SceneF_tc2.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_F_tc2.png -threshold 90% DF_maps/altered_thresh/limB_AE_F_tc2.png 
convert DF_maps/altered_thresh/no_AE_F_tc2.png -threshold 90% DF_maps/altered_thresh/no_AE_F_tc2.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_F_tc2.png -threshold 90% DF_maps/default_thresh/limB_AE_F_tc2.png 
convert DF_maps/default_thresh/no_AE_F_tc2.png -threshold 90% DF_maps/default_thresh/no_AE_F_tc2.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_F_tc2.png -gravity center -background black -extent 895x965 DF_maps/altered_thresh/limB_AE_F_tc2.png 
convert DF_maps/altered_thresh/no_AE_F_tc2.png -gravity center -background black -extent 895x965 DF_maps/altered_thresh/no_AE_F_tc2.png 
convert DF_maps/default_thresh/limB_AE_F_tc2.png -gravity center -background black -extent 895x965 DF_maps/default_thresh/limB_AE_F_tc2.png 
convert DF_maps/default_thresh/no_AE_F_tc2.png -gravity center -background black -extent 895x965 DF_maps/default_thresh/no_AE_F_tc2.png 
 
 
############## Ftc3 ############# 
 
# ground truth map: change transparency to be white background  instead, resize to largest image size of the four (from sizes of maps) with black extension 
convert DF_maps/SceneF_tc3.png -alpha remove -alpha off DF_maps/SceneF_tc3.png 
convert DF_maps/SceneF_tc3.png -resize 773x1019 -background black -gravity center -extent 773x1019 DF_maps/SceneF_tc3.png 
# altered thresh maps, go from greyscale to black and white 
convert DF_maps/altered_thresh/limB_AE_F_tc3.png -threshold 90% DF_maps/altered_thresh/limB_AE_F_tc3.png 
convert DF_maps/altered_thresh/no_AE_F_tc3.png -threshold 90% DF_maps/altered_thresh/no_AE_F_tc3.png 
 
# default thresh maps, go from greyscale to black and white 
convert DF_maps/default_thresh/limB_AE_F_tc3.png -threshold 90% DF_maps/default_thresh/limB_AE_F_tc3.png 
convert DF_maps/default_thresh/no_AE_F_tc3.png -threshold 90% DF_maps/default_thresh/no_AE_F_tc3.png 
 
# Extend the generated maps to fit largest image size of the four (from sizes of maps) 
convert DF_maps/altered_thresh/limB_AE_F_tc3.png -gravity center -background black -extent 773x1019 DF_maps/altered_thresh/limB_AE_F_tc3.png 
convert DF_maps/altered_thresh/no_AE_F_tc3.png -gravity center -background black -extent 773x1019 DF_maps/altered_thresh/no_AE_F_tc3.png 
convert DF_maps/default_thresh/limB_AE_F_tc3.png -gravity center -background black -extent 773x1019 DF_maps/default_thresh/limB_AE_F_tc3.png 
convert DF_maps/default_thresh/no_AE_F_tc3.png -gravity center -background black -extent 773x1019 DF_maps/default_thresh/no_AE_F_tc3.png 
 
 
