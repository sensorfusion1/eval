#!/bin/sh


for scene in 'Aa' 'Ab' 'Ac' 'Ba' 'Bb' 'Bc' 'Ca'
do
for case in 'nc1' 'nc2' 'nc3' 'tc1' 'tc2' 'tc3'
do
echo "----- $scene $case ----"
convert "ABC_maps/default_thresh/limB_AE_$scene""_$case"".png" -fuzz 1% -trim +repage "ABC_maps/default_thresh/limB_AE_$scene""_$case"".png"
convert "ABC_maps/default_thresh/no_AE_$scene""_$case"".png" -fuzz 1% -trim +repage "ABC_maps/default_thresh/no_AE_$scene""_$case"".png"

identify -format '%w, %h\n' "ABC_maps/default_thresh/limB_AE_$scene""_$case"".png"
identify -format '%w, %h\n' "ABC_maps/default_thresh/no_AE_$scene""_$case"".png"
done
done

for scene in 'Cb' 'Cc'
do
for case in 'nc1' 'nc2' 'nc3'
do
echo "----- $scene $case ----"
convert "ABC_maps/default_thresh/limB_AE_$scene""_$case"".png" -fuzz 1% -trim +repage "ABC_maps/default_thresh/limB_AE_$scene""_$case"".png"
convert "ABC_maps/default_thresh/no_AE_$scene""_$case"".png" -fuzz 1% -trim +repage "ABC_maps/default_thresh/no_AE_$scene""_$case"".png"

identify -format '%w, %h\n' "ABC_maps/default_thresh/limB_AE_$scene""_$case"".png"
identify -format '%w, %h\n' "ABC_maps/default_thresh/no_AE_$scene""_$case"".png"
done
done


for scene in 'Cb'
do
for case in 'tc1'
do
echo "----- $scene $case ----"
convert "ABC_maps/default_thresh/limB_AE_$scene""_$case"".png" -fuzz 1% -trim +repage "ABC_maps/default_thresh/limB_AE_$scene""_$case"".png"
convert "ABC_maps/default_thresh/no_AE_$scene""_$case"".png" -fuzz 1% -trim +repage "ABC_maps/default_thresh/no_AE_$scene""_$case"".png"

identify -format '%w, %h\n' "ABC_maps/default_thresh/limB_AE_$scene""_$case"".png"
identify -format '%w, %h\n' "ABC_maps/default_thresh/no_AE_$scene""_$case"".png"
done
done