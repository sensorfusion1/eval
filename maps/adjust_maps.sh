#!/bin/sh



# D nc #############

# ground truth map
convert DF_maps/SceneD_nc.png -background black -alpha remove -alpha off DF_maps/SceneD_nc.png
convert DF_maps/SceneD_nc.png -resize 575x1111 -background black -gravity center -extent 575x1111 DF_maps/SceneD_nc.png

# altered thresh maps, go from greyscale to black and white
convert DF_maps/altered_thresh/limB_AE_D_nc.png -threshold 90% DF_maps/altered_thresh/limB_AE_D_nc.png
convert DF_maps/altered_thresh/no_AE_D_nc.png -threshold 90% DF_maps/altered_thresh/no_AE_D_nc.png

# default thresh maps, go from greyscale to black and white
convert DF_maps/default_thresh/limB_AE_D_nc.png -threshold 90% DF_maps/default_thresh/limB_AE_D_nc.png
convert DF_maps/default_thresh/no_AE_D_nc.png -threshold 90% DF_maps/default_thresh/no_AE_D_nc.png

# Extend the generated maps to fit largest image size of the four (from sizes of maps)
convert DF_maps/altered_thresh/limB_AE_D_nc.png -gravity center -background black -extent 575x1111 DF_maps/altered_thresh/limB_AE_D_nc.png
convert DF_maps/altered_thresh/no_AE_D_nc.png -gravity center -background black -extent 575x1111 DF_maps/altered_thresh/no_AE_D_nc.png
convert DF_maps/default_thresh/limB_AE_D_nc.png -gravity center -background black -extent 575x1111 DF_maps/default_thresh/limB_AE_D_nc.png
convert DF_maps/default_thresh/no_AE_D_nc.png -gravity center -background black -extent 575x1111 DF_maps/default_thresh/no_AE_D_nc.png

# D tc1 #############
convert DF_maps/SceneD_tc1.png -background black -alpha remove -alpha off DF_maps/SceneD_tc1.png
convert DF_maps/SceneD_tc1.png -resize 570x820 -background black -gravity center -extent 570x820 DF_maps/SceneD_tc1.png

# D tc2 #############
convert DF_maps/SceneD_tc2.png -background black -alpha remove -alpha off DF_maps/SceneD_tc2.png
convert DF_maps/SceneD_tc2.png -resize 711x990 -background black -gravity center -extent 711x990 DF_maps/SceneD_tc2.png

# D tc3 #############
convert DF_maps/SceneD_tc3.png -background black -alpha remove -alpha off DF_maps/SceneD_tc3.png
convert DF_maps/SceneD_tc3.png -resize 750x1299 -background black -gravity center -extent 750x1299 DF_maps/SceneD_tc3.png

# E nc #############
convert DF_maps/SceneE_nc.png -background black -alpha remove -alpha off DF_maps/SceneE_nc.png
convert DF_maps/SceneE_nc.png -resize 470x747 -background black -gravity center -extent 470x747 DF_maps/SceneE_nc.png

# E tc1 #############
convert DF_maps/SceneE_tc1.png -background black -alpha remove -alpha off DF_maps/SceneE_tc1.png
convert DF_maps/SceneE_tc1.png -resize 1068x928 -background black -gravity center -extent 1068x928 DF_maps/SceneE_tc1.png

# E tc2 #############
convert DF_maps/SceneE_tc2.png -background black -alpha remove -alpha off DF_maps/SceneE_tc2.png
convert DF_maps/SceneE_tc2.png -resize 591x640 -background black -gravity center -extent 591x640 DF_maps/SceneE_tc2.png

# E tc3 #############
convert DF_maps/SceneE_tc3.png -background black -alpha remove -alpha off DF_maps/SceneE_tc3.png
convert DF_maps/SceneE_tc3.png -resize 945x919 -background black -gravity center -extent 945x919 DF_maps/SceneE_tc3.png

# F nc #############
convert DF_maps/SceneF_nc.png -background black -alpha remove -alpha off DF_maps/SceneF_nc.png
convert DF_maps/SceneF_nc.png -resize 483x765 -background black -gravity center -extent 483x765 DF_maps/SceneF_nc.png

# F tc1 #############
convert DF_maps/SceneF_tc1.png -background black -alpha remove -alpha off DF_maps/SceneF_tc1.png
convert DF_maps/SceneF_tc1.png -resize 648x925 -background black -gravity center -extent 648x925 DF_maps/SceneF_tc1.png

# F tc2 #############
convert DF_maps/SceneF_tc2.png -background black -alpha remove -alpha off DF_maps/SceneF_tc2.png
convert DF_maps/SceneF_tc2.png -resize 895x965 -background black -gravity center -extent 895x965 DF_maps/SceneF_tc2.png

# F tc3 #############
convert DF_maps/SceneF_tc3.png -background black -alpha remove -alpha off DF_maps/SceneF_tc3.png
convert DF_maps/SceneF_tc3.png -resize 773x1019 -background black -gravity center -extent 773x1019 DF_maps/SceneF_tc3.png






