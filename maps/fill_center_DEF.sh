#!/bin/sh

# Img size: 500x500



for scene in 'D' 'E' 'F'
do
for case in 'nc' 'tc1' 'tc2' 'tc3'
do
echo "----- $scene $case ----"

convert "DEF_maps/default_thresh/limB_AE_$scene""_$case"".png" -background black -gravity center -extent 500x500 "DEF_maps/default_thresh/limB_AE_$scene""_$case"".png"
convert "DEF_maps/default_thresh/no_AE_$scene""_$case"".png" -background black -gravity center -extent 500x500 "DEF_maps/default_thresh/no_AE_$scene""_$case"".png"


convert "DEF_maps/ground_truths/Scene$scene""_$case"".png" -fuzz 1% -trim +repage "DEF_maps/ground_truths/Scene$scene""_$case"".png"
convert "DEF_maps/ground_truths/Scene$scene""_$case"".png" -background black -gravity center -extent 500x500 "DEF_maps/ground_truths/Scene$scene""_$case"".png"
done
done


