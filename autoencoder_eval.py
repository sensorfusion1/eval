#!/usr/bin/env python3

print('Autoencoder evaluation')
import os
import sys
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import numpy as np
import keras
sys.path.insert(1, os.path.expanduser('~/labeling'))
from load_npy_data import load_npy_scene
sys.path.insert(1, os.path.expanduser('~/new_network/scripts'))
from help_functions import clean_data
from keras.utils import to_categorical
from keras.metrics import CategoricalAccuracy, Precision, Recall

################################################
## Load autoencoder model and scenes
################################################

network_dir = os.path.expanduser('~/new_network')
detector = keras.models.load_model(network_dir + '/detector_model/detector_lim_batches')

labeling_dir = os.path.expanduser('~/labeling')
sceneA_labels, sceneA = load_npy_scene('A')
sceneB_labels, sceneB = load_npy_scene('B')
sceneC_labels, sceneC = load_npy_scene('C')

sceneA_labels = to_categorical(sceneA_labels)
sceneB_labels = to_categorical(sceneB_labels)
sceneC_labels = to_categorical(sceneC_labels)

# ###############################################
# Process data + predictions
# ###############################################

# Preprocess the data (clean_data)
sceneA = np.nan_to_num(sceneA, nan=1e10)
sceneB = np.nan_to_num(sceneB, nan=1e10)
sceneC = np.nan_to_num(sceneC, nan=1e10)
sceneA = clean_data(sceneA)
sceneB = clean_data(sceneB)
sceneC = clean_data(sceneC)

# # Perform prediction
# predictA = detector.predict(sceneA, batch_size=784)
# predictB = detector.predict(sceneB, batch_size=784)
# predictC = detector.predict(sceneC, batch_size=784)

# print('predict A shape', predictA.shape,'predict B shape',predictB.shape,'predict C shape',predictC.shape)

##########################################
# Performance
##########################################
print('--------Performance----------')

detector.compile(loss='categorical_crossentropy',
        metrics=['categorical_accuracy','Precision','Recall'])

print('\nScene A')
eval_detect_A = detector.evaluate(sceneA, sceneA_labels)
print("Loss: ", eval_detect_A[0], "\tCategorical accuracy: ", eval_detect_A[1], "\tPrecision: ", eval_detect_A[2], "\tRecall: ", eval_detect_A[3])

print('\nScene B')
eval_detect_B = detector.evaluate(sceneB, sceneB_labels)
print("Loss: ", eval_detect_B[0], "\tCategorical accuracy: ", eval_detect_B[1], "\tPrecision: ", eval_detect_B[2], "\tRecall: ", eval_detect_B[3])

print('\nScene C')
eval_detect_C = detector.evaluate(sceneC, sceneC_labels)
print("Loss: ", eval_detect_C[0], "\tCategorical accuracy: ", eval_detect_C[1], "\tPrecision: ", eval_detect_C[2], "\tRecall: ", eval_detect_C[3])