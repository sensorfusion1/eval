#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import os
import pathlib

print('Execution time evaluation')

save = True

# Directory
exec_time_dir = os.path.expanduser('~/eval/exec_time/')
save_dir = os.path.expanduser('~/eval/exec_plots/')

# Customise outlier data appearence
flierprops = dict(marker='.', markerfacecolor='b', markersize=2,linestyle='none', markeredgecolor='b')

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color='red')

# For the differently coloured boxplots
# 1 1.5    2.5 3    4 4.5   5.5 6
pos = [[1,2.5,4,5.5],[1.5,3,4.5,6]]
colors = ['orange','blue','green','black']

################################################
# Plot all cases per scene
################################################

# Generating scene-case-index dict
scenes = ['Aa','Ab','Ac','Ba','Bb','Bc','Ca','Cb','Cc','D','E','F']
cases = {'ab':['nc1','nc2','nc3','tc1','tc2','tc3'],'c':['nc1','nc2','nc3'],'def':['nc','tc1','tc2','tc3']}


minor_locator = AutoMinorLocator(5)
exec_time = dict()
for i,scene in enumerate(scenes[:9]):
    for j,case in enumerate(cases['ab']):
        
        try:
            filename = 'limB_AE_' + scene + '_' + case
            df = pd.read_csv(exec_time_dir + filename + '.csv', sep='\t', header = 0)
            exec_time[scene+case] = df['Time (s)'].values[1:]
            # print(scene,case)
        except:
            pass

for i,scene in enumerate(scenes[9:]):
    for j,case in enumerate(cases['def']):
        
        filename = 'limB_AE_' + scene + '_' + case
        df = pd.read_csv(exec_time_dir + filename + '.csv', sep='\t', header = 0)

        exec_time[scene+case] = df['Time (s)'].values[1:]
        # print(scene,case)

## #############
## Scenes D E F
## #############
# exec_time = dict()
fig1, axs = plt.subplots(len(scenes[9:]),figsize=(8,10))
axs = axs.ravel()

for i,scene in enumerate(scenes[9:]):
    for j,case in enumerate(cases['def']):
        
        # filename = 'limB_AE_' + scene + '_' + case
        # df = pd.read_csv(exec_time_dir + filename + '.csv', sep='\t', header = 0)

        # exec_time[scene+case] = df['Time (s)'].values[1:]
        axs[i].boxplot(exec_time[scene+case],showfliers=True, flierprops=flierprops, positions = [j]) # boxplot of each case per scene, suppressing the outliers via showfliers param
    axs[i].set_title('Scene' + scene)
    axs[i].yaxis.set_minor_locator(minor_locator)
    axs[i].yaxis.grid(which='both')

fig1.suptitle('Execution Time of Autoencoder per All Scenes and Cases')
plt.setp(axs,xticks=[0, 1, 2,3], xticklabels=cases['def'],xlim=[-0.2,3.2],yticks = np.arange(0, 0.21, 0.05),ylabel='Time (s)',ylim=[0,0.21])
  
# # show plot
plt.tight_layout(pad=2)
plt.subplots_adjust(top=0.92)



################################################
# Plot per case
################################################
# Sort data per case basis
cases_dict = {key: list() for key in cases['def']}
for scene in scenes[9:]:
    for case in cases['def']:
        cases_dict[case].extend(exec_time[scene+case])

# Initiate plot
fig2, ax2 = plt.subplots(figsize=(8,5))

for i,case in enumerate(cases['def']):
    ax2.boxplot(cases_dict[case],showfliers=True, flierprops=flierprops, positions = [i]) # boxplot of each case, suppressing the outliers via showfliers param
ax2.yaxis.set_minor_locator(minor_locator) # grid resolution
plt.grid(axis='y',which='both')

plt.setp(ax2,xticks=[0, 1, 2,3], xticklabels=cases['def'],xlim=[-0.2,3.2],yticks = np.arange(0, 0.21, 0.05),ylabel='Time (s)',ylim=[0,0.21],
            title='Execution Time of Autoencoder per Case')
  
# show plot
plt.tight_layout(pad=2)


################################################
# Plot total execution time
################################################
exec_time_tot = list()
for case in cases['def']:
    exec_time_tot.extend(cases_dict[case])

# Initiate plot
fig3, ax3 = plt.subplots(figsize=(8,5))

ax3.boxplot(exec_time_tot, showfliers=True,flierprops=flierprops,positions=[0])
ax3.yaxis.set_minor_locator(minor_locator) # grid resolution
plt.grid(axis='y',which='both')
plt.setp(ax3,xticks=[0], xticklabels=['Tot'],yticks = np.arange(0, 0.21, 0.05),ylabel='Time (s)',ylim=[0,0.21],
            title='Total Execution Time of Autoencoder')
  
plt.tight_layout(pad=2)
title = 'Total'

#####################################################################################################################################

## #############
## Scenes B C
## #############

cases_dict = {key: list() for key in cases['def']}
for scene in scenes[3:9]:
    for case in cases['ab']:
        if 'nc' in case:
            cases_dict['nc'].extend(exec_time[scene+case])
            print(scene,case)
        else:
            try:
                cases_dict[case].extend(exec_time[scene+case])
                print(scene,case)
            except:
                pass

print(cases_dict.keys())

## ###############
## Scenes A D E F
## ###############
print('g',['nc']+cases['ab'])
cases_dict_adef = {key: list() for key in cases['def']}
for scene in scenes[:3]+scenes[9:]:
    for case in cases['def']:
        try:
            cases_dict_adef[case].extend(exec_time[scene+case])
            print(scene,case)
        except:
            for c in ['nc1','nc2','nc3']:
                cases_dict_adef['nc'].extend(exec_time[scene+c])
                print('except',scene,c)


print(cases_dict_adef.keys())

##########################################################
# Plot BC vs ADEF
fig4, ax4 = plt.subplots(figsize=(8,10))
# BC
for i,case in enumerate(cases_dict.keys()):
    # print('i',i,'case',case,'pos',pos[0][i])
    bp = ax4.boxplot(cases_dict[case],showfliers=True, flierprops=flierprops, positions = [pos[0][i]])
    set_box_color(bp,'blue')

# ADEF
for i,case in enumerate(cases_dict_adef.keys()):
    bp = ax4.boxplot(cases_dict_adef[case],showfliers=True, flierprops=flierprops, positions = [pos[1][i]]) 
    set_box_color(bp,'black')

# Legend
l_colors = ['blue', 'black']
for i,case in enumerate(['BC', 'ADEF']):
    plt.plot([], c=l_colors[i], label=case) # draw temporary lines and use them to create a legend
plt.legend()


ax4.yaxis.set_minor_locator(minor_locator)
ax4.yaxis.grid(which='both')

fig4.suptitle('Execution Time of Autoencoder BC vs ADEF')

plt.setp(ax4,xticks=[1.25,2.75,4.25,5.75], xticklabels=cases['def'],xlim=[0.2,7],yticks = np.arange(0, 0.21, 0.05),ylabel='Time (s)',ylim=[0,0.21])
  


plt.tight_layout(pad=2)
plt.subplots_adjust(top=0.92)







# # ################################################
# # Plot test vs training datasets
# ################################################



if save:
    # fig1.savefig(exec_time_dir+'Scenes_cases'+'.jpg', bbox_inches='tight')
    # fig2.savefig(exec_time_dir+'Cases'+'.jpg', bbox_inches='tight')
    # fig3.savefig(exec_time_dir+title+'.jpg', bbox_inches='tight')

    pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True)
    fig1.savefig(save_dir + 'exec_per_scene_case.png', bbox_inches='tight')
    fig2.savefig(save_dir + 'exec_per_case.png', bbox_inches='tight')
    fig3.savefig(save_dir + 'total_exec_time.png', bbox_inches='tight')
    fig4.savefig(save_dir + 'exec_bc_vs_adef.png', bbox_inches='tight')
else:
    plt.show()
