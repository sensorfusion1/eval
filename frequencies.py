import pandas as pd
import os
import pathlib
import matplotlib
import matplotlib.pyplot as plt
import numpy as np


SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 18

plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


save_dir = os.path.expanduser('~/eval/frequencies/')
data_dir = os.path.expanduser('~/eval/')
save = True


## Prepare DF
df = pd.read_csv(data_dir + 'frequencies.csv', header = [0, 1, 2, 3])

df.columns = df.columns.rename(['scene', 'ae', 'tc', 'metric'])


################### TC1 #########################

df_true_lid = df.copy()
df_false_lid = df.copy()

# print(df_false_lid)

tc1_scenes = ['Aa', 'Ab', 'Ac', 'Ba', 'Bb', 'Bc', 'Ca', 'Cb', 'D', 'E', 'F']
tc1_angles = ['from_angle', 'to_angle', 'side']

L_true = {'from_angle':[], 'to_angle': [], 'side': []}
L_false = {'from_angle':[], 'to_angle': [], 'side': []}

# print(df_false_lid[(:, 'TRUE', 'tc2', :)])

for sc in tc1_scenes:
    for m in tc1_angles:
        for i in df_true_lid[(sc, 'TRUE', 'tc1', m)].dropna().values:
            L_true[m].append(i)

for sc in tc1_scenes:
    for m in tc1_angles:
        for i in df_false_lid[(sc, 'FALSE', 'tc1', m)].dropna().values:
            L_false[m].append(i)


L_true = pd.DataFrame(L_true)
L_false = pd.DataFrame(L_false)


L_true_hist = []
L_false_hist = []

# print('....')
for i in L_true.values:
    start = min(int(i[0]), int(i[1]))
    stopp = max(int(i[0]), int(i[1]))
    angs = range(start, stopp+1)
    for j in angs:
        if j > 90:
            L_true_hist.append(90-(j-90))
        else:
            L_true_hist.append(j)

for i in L_false.values:
    start = min(int(i[0]), int(i[1]))
    stopp = max(int(i[0]), int(i[1]))
    angs = range(start, stopp)
    for j in angs:
        if j > 90:
            L_false_hist.append(90-(j-90))
        else:
            L_false_hist.append(j)    


fig_L_true_hist, axs = plt.subplots(1, 1)
axs.hist(L_true_hist, 91)
axs.set_title('AE true')
axs.set_xticks(np.arange(0, 91, 45))
axs.plot([90.05, 90.05], [0, 60], color='black', alpha=0.8, label='90 degrees', linestyle='dashed')
axs.set_xlabel('Angle')
axs.set_ylabel('Frequency')

fig_L_false_hist, axs = plt.subplots(1, 1)
axs.hist(L_false_hist, 91)
axs.set_title('AE false')
axs.set_xticks(np.arange(0, 91, 45))
axs.plot([90.05, 90.05], [0, 60], color='black', alpha=0.8, label='90 degrees', linestyle='dashed')
axs.set_xlabel('Angle')
axs.set_ylabel('Frequency')

print(L_false)

plt.show()

# print(L_true)
# print('--------')
# print(L_false)


# print('______')

# print(L_false['side'].value_counts())
# print(L_true['side'].value_counts())

tc1_tot_side = pd.concat([L_true['side'].value_counts(), L_false['side'].value_counts()], axis=1)
# print(tc1_tot_side)


####### Total occurences left right TC1 ##################

fig_tc1_tot, ax = plt.subplots(1, 1, figsize=(7, 7))
tc1_tot_side.plot(kind='bar', ax=ax, rot=20)
ax.set_xticklabels(['Right', 'Left'])
ax.legend(['With AE', 'Without AE'])
ax.yaxis.grid(True, linestyle='dotted')
ax.set_title('Total occurences')

L_true['ang_diff'] = L_true['to_angle'] - L_true['from_angle']
L_false['ang_diff'] = L_false['to_angle'] - L_false['from_angle']


# print(L_true)

################ From and to angle TC1 ######################

L_true = L_true.sort_values(by='from_angle')
L_false = L_false.sort_values(by='from_angle')

fig_angles_withAE, axs = plt.subplots(1, 1, figsize=(10,10))
x_true = range(len(L_true['from_angle']))
x_false = range(len(L_false['from_angle']))


axs.hlines(y=x_true, xmin=L_true['from_angle'], xmax=L_true['to_angle'])
axs.scatter(L_true['from_angle'], x_true, color='navy', alpha=1, label='From angle')
axs.scatter(L_true['to_angle'], x_true, color='gold', alpha=0.8, label='To angle')
axs.set_title('With AE')
axs.plot([90, 90], [-1, 82], color='red', alpha=0.8, label='90 degrees')
axs.legend(loc='lower right')
axs.set_xlabel('Angle')
axs.set_ylabel('Sample')


##############
fig_angles_without, axs = plt.subplots(1, 1, figsize=(10,10))

axs.hlines(y=x_false, xmin=L_false['from_angle'], xmax=L_false['to_angle'])
axs.scatter(L_false['from_angle'], x_false, color='navy', alpha=1, label='From angle')
axs.scatter(L_false['to_angle'], x_false, color='gold', alpha=0.8, label='To angle')
axs.plot([90, 90], [-1, 82], color='red', alpha=0.8, label='90 degrees')
axs.set_title('Without AE')
axs.set_xlabel('Angle')
axs.set_ylabel('Sample')
axs.legend(loc='lower right')


##############
fig_boxplots_to_from_lidar, axs = plt.subplots(1, 1, figsize=(10,10))

L_true.columns = ['With AE from_angle', 'With AE to_angle', 'With AE side', 'With AE diff']
L_false.columns = ['Without AE from_angle', 'Without AE to_angle', 'Without AE side', 'Without AE diff']

L_boxes = pd.concat([L_true, L_false], axis = 1)
# print(L_boxes)


boxplots_from_to = L_boxes.boxplot(column=['With AE from_angle', 'With AE to_angle', 'Without AE from_angle', 'Without AE to_angle'], rot=20)

fig_boxplots_lidar_diff, axs = plt.subplots(1, 1, figsize=(10,10))
boxplot_diff = L_boxes.boxplot(column=['With AE diff', 'Without AE diff'], ax = axs)

############### From and to angle TC1 Histogram ######################



# ################### TC2 #########################

df_true_cam = df.copy()
df_false_cam = df.copy()

D_true = {}
D_false = {}
tc2_scenes = ['Aa', 'Ab', 'Ac', 'Ba', 'Bb', 'Bc', 'Ca', 'D', 'E', 'F']
camera_cases = ['b', 't', 'p', 'r', 'g']
camera_types = ['s', 'u', 'f']

for c in camera_cases:
    D_true[c] = {}
    D_false[c] = {}
    for t in camera_types:
        D_true[c][t] = 0
        D_false[c][t] = 0


for sc in tc2_scenes:
    for c in camera_cases:
        temp1 = df_true_cam[(sc, 'TRUE', 'tc2', 'case')] == c
      
        for t in camera_types:
            temp = df_true_cam[(sc, 'TRUE', 'tc2', 'type')].str.contains(t)

            D_true[c][t] = D_true[c][t] + (temp*temp1).sum()
    


for sc in tc2_scenes:
    for c in camera_cases:
        temp1 = df_false_cam[(sc, 'FALSE', 'tc2', 'case')] == c
      
        for t in camera_types:
            temp = df_false_cam[(sc, 'FALSE', 'tc2', 'type')].str.contains(t)

            D_false[c][t] = D_false[c][t] + (temp*temp1).sum()

# print('ae false: ', D_false)
# print('ae true: ', D_true)

D_true = pd.DataFrame(D_true).transpose()
D_false = pd.DataFrame(D_false).transpose()

########## Types per case TC2 #######################

fig_tc2_per_type_AE, axes = plt.subplots(1, 1, figsize=(10, 7))

D_true.plot(ax=axes, kind='bar', rot=20)
axes.set_xticklabels(['Black', 'Turquoise', 'Pink', 'Red', 'Green'])
axes.yaxis.grid(True, linestyle='dotted')
axes.set_title('With AE')
axes.legend(['Shifted', 'Unstable', 'Flyout'])
axes.set_ylim([0, 20])
axes.set_yticks(np.arange(0, 20, step=2))


#####
fig_tc2_per_type_without, axes = plt.subplots(1, 1, figsize=(10, 7))
D_false.plot(ax=axes, kind='bar', rot = 20)
axes.set_xticklabels(['Black', 'Turquoise', 'Pink', 'Red', 'Green'])
axes.yaxis.grid(True, linestyle='dotted')
axes.set_title('Without AE')
axes.legend(['Shifted', 'Unstable', 'Flyout'])
axes.set_ylim([0, 20])
axes.set_yticks(np.arange(0, 20, step=2))


D_true['sum'] = D_true.sum(axis=1)
D_false['sum'] = D_false.sum(axis=1)


tot_ae = D_true['sum']
tot_none = D_false['sum']
# print(tot_ae)

tc2_tot = pd.concat([tot_ae, tot_none], axis=1)
# print(tc2_tot)


########## Total occurences TC2 ##################

fig_tc2_tot, ax = plt.subplots(1, 1, figsize=(7, 7))
tc2_tot.plot(kind='bar', ax=ax, rot=20)
ax.set_xticklabels(['Black', 'Turquoise', 'Pink', 'Red', 'Green'])
ax.legend(['With AE', 'Without AE'])
ax.yaxis.grid(True, linestyle='dotted')
ax.set_title('Total occurences')



if save:
    pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True)
    fig_angles_withAE.savefig(save_dir + 'angles_to_and_from_AE.png', bbox_inches='tight')
    fig_angles_without.savefig(save_dir + 'angles_to_and_from_without.png', bbox_inches='tight')
    fig_tc1_tot.savefig(save_dir + 'total_cases_tc1.png', bbox_inches='tight')
    fig_tc2_per_type_AE.savefig(save_dir + 'types_per_case_tc2_AE.png', bbox_inches='tight')
    fig_tc2_per_type_without.savefig(save_dir + 'types_per_case_tc2_without.png', bbox_inches='tight')
    fig_tc2_tot.savefig(save_dir + 'total_cases_tc2.png', bbox_inches='tight')
    fig_boxplots_lidar_diff.savefig(save_dir + 'boxplots_lidar_diff', bbox_inches='tight')
    fig_boxplots_to_from_lidar.savefig(save_dir + 'boxplot_lidar_to_from', bbox_inches='tight')
    fig_L_true_hist.savefig(save_dir+'histogram_ae_true', bbox_inches='tight')
    fig_L_false_hist.savefig(save_dir+'histogram_ae_false', bbox_inches='tight')

else:
    plt.show()